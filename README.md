
# UBUNTU
### Shortcuts :
| Purpose | Shortcut |
| --------|----------|
| Terminal|Ctrl+Alt+T|
---


## Preferences
Preparations
```sh
sudo apt update
sudo apt install software-properties-common apt-transport-https wget
```
---

### VsCode
```sh
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
sudo apt update
sudo apt install code
```
---




### Chrome Browser
```sh
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb
```
---



### Dash-to-dock icons
(Better to do after software installations)
```sh
gsettings set org.gnome.shell favorite-apps "['org.gnome.Terminal.desktop', 'code.desktop', 'org.gnome.Nautilus.desktop', 'google-chrome.desktop', 'org.gnome.Software.desktop', 'gnome-control-center.desktop']"
```
---


### Install : 
- NVM
  ```sh
  wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.2/install.sh | bash
  ```
- GIT
  ```sh
  sudo apt update
  sudo apt install git
  ```



---
# MacOS
Install :
- HomeBrew
  ```sh
  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  ```
- NVM using Brew
  ```sh
  brew update
  brew install nvm
  mkdir ~/.nvm
  nano ~/.bash_profile
  ```
  inside the file paste the following :
  ```sh
  export NVM_DIR=~/.nvm
  source $(brew --prefix nvm)/nvm.sh
  ```
  to activate just do this in the terminal
  ```sh
  source ~/.bash_profile
  echo $NVM_DIR
  ```




---
## NODE.JS
```sh
nvm install 12.14.0
```



---
## VSCODE
Sync VsCode's settings with "Settings Sync" extension




---
# PROJECT IT SELF
1. Login [Bitbucket](https://bitbucket.org/enjoydevteam/enjoy-dev-env/src/master/)
2. Create a general purpose directory in Home
    ```sh
    mkdir ~/ENJOY
    ```
3. Open the directory with VsCode
    ```sh
    code ~/ENJOY -r
    ```
4. Within VsCode's terminal, clone the Dev Environment repo.
   (it will ask for the password, it's the repo's password, not the ubuntu one)
    ```sh
    git clone https://Nateegeek@bitbucket.org/enjoydevteam/enjoy-dev-env.git
    ```
5. Install these global npm modules
    ```sh
    npm install -g ionic cordova cordova-res jest
    ```
6. CD inside "Enjoy TOC" directory and run
    ```sh
    npm i
    npm i -D @ionic/angular-toolkit 
    npm i -D @angular-devkit/build-angular
    ```
7. Android building tools
   - follow the [guide](https://cordova.apache.org/docs/en/latest/guide/platforms/android/index.html#requirements-and-support)
   - Add these paths to System's Path
     - C:\sdk-tools-windows-4333796\platform-tools
     - C:\sdk-tools-windows-4333796\tools\bin
     - C:\sdk-tools-windows-4333796\tools
     - C:\Program Files\Java\jre1.8.0_231\bin
     - C:\Program Files\Java\jdk1.8.0_231\bin
     - C:\sdk-tools-windows-4333796\build-tools\29.0.2