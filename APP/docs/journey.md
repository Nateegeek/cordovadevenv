
## Start
```sh
ionic start Enjoy-App sidemenu --cordova
```

## Replace example values with our info
change
```xml
<widget id="io.ionic.starter"
```
into
change
```xml
<widget id="enjoy.enjoytoc.app"
```


## Add platforms
```sh
ionic cordova platform add android
```

### FIX "MissingDefaultResource" adding the following in ./config.xml
```xml
<splash density="ldpi"    src="resources/android/splash/drawable-port-ldpi-screen.png"    />
<splash density="mdpi"    src="resources/android/splash/drawable-port-mdpi-screen.png"    />
<splash density="hdpi"    src="resources/android/splash/drawable-port-hdpi-screen.png"    />
<splash density="xhdpi"   src="resources/android/splash/drawable-port-xhdpi-screen.png"   />
<splash density="xxhdpi"  src="resources/android/splash/drawable-port-xxhdpi-screen.png"  />
<splash density="xxxhdpi" src="resources/android/splash/drawable-port-xxxhdpi-screen.png" />
```
---





## Build
```sh
ionic build --prod --engine=cordova --platform=android
ionic cordova build android
```
---





## Generate a private key
### FIX "'keytool' is not recognized as an internal or external command"
- add this to PATH
  ```sh
  C:\Program Files\Java\jre1.8.0_231\bin
  ```

Inside the App project directory, run
```sh
keytool -genkey -v -keystore enjoy-toc-release-key.keystore -alias enjoy_toc -keyalg RSA -keysize 2048 -validity 10000
```


## Signing an APK
```sh
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore enjoy-toc-release-key.keystore platforms\android\app\build\outputs\apk\release\app-release-unsigned.apk enjoy_toc
```
---



## ZipAlign
```sh
zipalign -v 4 platforms\android\app\build\outputs\apk\release\app-release-unsigned.apk platforms\android\app\build\outputs\apk\release\enjoy_toc.apk
```

