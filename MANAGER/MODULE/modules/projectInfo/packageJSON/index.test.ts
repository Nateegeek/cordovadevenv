import Winston from 'winston';
import Transport from 'winston-transport';
import { PackageJSON } from ".";
import { FS, Context, Logger, LogObject } from '../../common/common.interfaces';
import { PackageJSONData } from './packageJSON.interfaces';




class MockedFS implements FS {

  constructor() {
    this.existsSync    = jest.fn(this.existsSync   );
    this.readFileSync  = jest.fn(this.readFileSync );
    this.writeFileSync = jest.fn(this.writeFileSync);
  }

     existsSync = jest.fn((filePath: string) => true);

   readFileSync = jest.fn((filePath: string, encoding: string) => ``);

  writeFileSync = jest.fn((filePath: string,  content: string) => {});
}




class MockedTransport extends Transport {
  constructor(public logs: LogObject[]) {
    super();
  }

  log(info: LogObject, callback: () => void) {
    this.logs.push(info);

    callback();
  }
}





describe(`PackageJSON`, () => {
  const filePath = 'imaginary/path';
  


  describe(`contructor`, () => {
    it(`warns when file does not exist`, () => {
      const logger = {} as Logger;
      const mockedWarn  = logger.warn  = jest.fn();
      const mockedChild = logger.child = jest.fn().mockReturnValue(logger);
      const context: Context = { logger };

  
      const mockedFileExists = jest.fn();
      
      // no warn if file does exist
      mockedFileExists.mockReturnValue(true);
      PackageJSON.prototype.fileExists = mockedFileExists;
      new PackageJSON(filePath, null, context);
      
      expect(mockedWarn.mock.calls).toHaveLength(0);
  
  
      // warn if file does not exist
      mockedFileExists.mockReturnValue(false);
      new PackageJSON(filePath, null, context);
      
      expect(mockedWarn.mock.calls).toHaveLength(1);
      expect(mockedWarn.mock.calls[0][0]).toMatchObject({
        vars: { filePath },
        methodName: 'constructor'
      });

    });
  });



  describe(`get`, () => {
    const logger = {} as Logger;
    const mockedDebug = logger.debug = jest.fn();
    const mockedError = logger.error = jest.fn();
    const mockedChild = logger.child = jest.fn().mockReturnValue(logger);
    const context: Context = { logger };

    const mockedFileExists = jest.fn();
      
    // no warn if file does exist
    mockedFileExists.mockReturnValue(true);
    PackageJSON.prototype.fileExists = mockedFileExists;

    const fs = new MockedFS();

    const packageJSON = new PackageJSON(filePath, fs, context);


    it(`Parse and return a "PackageJSONData"`, () => {
      const data = { name: `test name`, version: `0.0.1` };
      const fileContent = JSON.stringify(data, null, 2);
      fs.readFileSync.mockReturnValue(fileContent);
      const returnedData = packageJSON.get();

      expect(returnedData).toEqual(data);
    });


    it(`Throws exception and Logs when "fs.readFileSync()" fails`, () => {
      fs.readFileSync.mockImplementation(() => { throw new Error();} );
      expect(() => packageJSON.get()).toThrowError();
      expect(mockedError).toBeCalledWith(
        expect.objectContaining({
          vars: {
            filePath
          }
        })
      );
    });

  });



  describe(`set`, () => {
    const logger = {} as Logger;
    const mockedDebug = logger.debug = jest.fn();
    const mockedError = logger.error = jest.fn();
    const mockedChild = logger.child = jest.fn().mockReturnValue(logger);
    const context: Context = { logger };

    const fs = new MockedFS();

    const packageJSON = new PackageJSON(filePath, fs, context);

    it(`Stringify and write in the file`, () => {
      const data = { name: `test name`, version: `0.0.1` } as PackageJSONData;
      const expectedFileContent = JSON.stringify(data, null, 2);
      fs.writeFileSync.mockReset();
      packageJSON.set(data);
      expect(fs.writeFileSync).toBeCalledWith(filePath, expectedFileContent);

      expect(mockedDebug).toBeCalledWith(
        expect.objectContaining({
          vars: {
            filePath,
            fileContent: expectedFileContent
          },
          methodName: 'set'
        })
      );
    });
  });

});
