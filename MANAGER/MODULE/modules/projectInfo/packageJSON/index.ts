import { FS, Context, Logger } from '../../common/common.interfaces';
import { PackageJSONData     } from './packageJSON.interfaces';






export class PackageJSON {

  private logger: Logger;



  constructor(
    public  filePath: string,
    private fs: FS,
    context: Context
  ) {

    this.logger = context.logger.child({ className: 'PackageJSON' });

    if (!this.fileExists()) {
      this.logger.warn({
        message: `PackageJSON.filePath does not exist`,
        vars: { filePath },
        methodName: 'constructor'
      });
    }
  }




  fileExists(): boolean {
    const fileExists = this.fs.existsSync(this.filePath);
    this.logger.debug({ message: `fileExists`, vars: { fileExists }, methodName: 'fileExists'});
    
    return fileExists;
  }




  get(): PackageJSONData {
    const filePath = this.filePath;

    let fileContent: string;
    let parsedData: PackageJSONData;


    const logger = this.logger.child({ methodName: 'get' });


    try {
      fileContent = this.fs.readFileSync(filePath, 'utf8');
      logger.debug({ message: `fileContent`, vars: { fileContent } });

    } catch(error) { logger.error({ message: ``, error, vars: { filePath } }); throw error; }


    try {
      parsedData = JSON.parse(fileContent);
      logger.debug({ message: `parsedData`, vars: { parsedData } });

    } catch(error) { logger.error({ message: ``, error, vars: { fileContent } }); throw error; }

  
    return parsedData;
  }



  set(data: PackageJSONData): void {
    let fileContent: string;

    try {
      fileContent = JSON.stringify(data, null, 2);
      this.logger.debug({ message: ``, vars: { fileContent }, methodName: 'set'});

    } catch(error) { this.logger.error({ message: ``, vars: { data }, error, methodName: 'set'}); throw error; }

    try {
      this.fs.writeFileSync(this.filePath, fileContent);
      this.logger.debug({
        message: `fileContent has been written in the file`,
        vars: {
          filePath: this.filePath,
          fileContent
        },
        methodName: 'set'
      });

    } catch(error) { this.logger.error({ message: ``, vars: { fileContent }, error, methodName: 'set'}); throw error; }
  }



}
