export const PackageJSONPropTypes = {
  name:        'string',
  version:     'string',
  description: 'string'
}
export interface PackageJSONData {
  name:        string;
  version:     string;
  description: string;
};
