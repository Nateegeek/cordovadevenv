import isPlainObject from 'lodash.isplainobject';

import { ReadPackageJSON, WritePackageJSON, PackageJSONPropTypes } from '../projectInfo/packageJSON';
import { ReadConfigXML,   WriteConfigXML } from '../projectInfo/configXML';

import { Context } from '../common/common.interfaces';
import { ProjectInfoData, ProjectInfoPropName, ProjectInfoDependencies } from './projectInfo.interfaces';
import { returnEveryProperties } from './getters/getEveryProperties';

  
  
  
  









/**
 * Factory function that returns projectInfo()
 * @param context 
 */
export function ProjectInfo(context: Context): typeof projectInfo {
  const  readPackageJSON =  ReadPackageJSON(context);
  const writePackageJSON = WritePackageJSON(context);

  const    readConfigXML =    ReadConfigXML(context);
  const   writeConfigXML =   WriteConfigXML(context);

  const dependencies: ProjectInfoDependencies = { readPackageJSON, writePackageJSON,
                                                  readConfigXML,   writeConfigXML};

  return projectInfo.bind({ context, dependencies });
} 









/**
 * Get every properties
 * @variation 1
 */
function projectInfo(): Promise<ProjectInfoData>


/**
 * Get a specific property
 * @param propName
 * @variation 2
 */
function projectInfo<N extends ProjectInfoPropName>(propName: N): Promise<ProjectInfoData[N]>


/**
 * Set a specific property
 * @param propName
 * @param value 
 * @variation 3
 */
function projectInfo<N extends ProjectInfoPropName, V extends ProjectInfoData[N]>(propName: N, value: V): Promise<void>


/**
 * Set multiple properties
 * @param keyValueObj 
 * @variation 4
 */
function projectInfo(keyValueObj: Partial<ProjectInfoData>): Promise<void>


/**
 * @link projectInfo(1)
 * @param args 
 */
function projectInfo<N extends ProjectInfoPropName, V extends ProjectInfoData[N]>(arg1?: N | Partial<ProjectInfoData>, arg2?: V): any {
  const context: Context = this.context;
  const dependencies: ProjectInfoDependencies = this.dependencies;

  return Promise.all([
    dependencies.readPackageJSON(),
    dependencies.readConfigXML()
  ]).then(async ([packageJSONData, configXMLData]) => {
    const typeArg1 = typeof arg1;
    const typeArg2 = typeof arg2;

    if (typeArg1 === 'undefined' && typeArg2 === 'undefined') { // @variation 1: Get every properties
      return returnEveryProperties(packageJSONData, configXMLData);
    } else {
      if (typeArg2 !== 'undefined') { // @variation 3: Set a specific property
        if (typeArg1 !== 'string') throw Error(`"propName" must be a string`);

        const  name = arg1 as ProjectInfoPropName;
        const value = arg2;

        switch (name) {
          case 'name':
            configXMLData.widget.name = value;
            dependencies.writeConfigXML(configXMLData);
            break;


          case 'packageName': 
            packageJSONData.name = value;
            dependencies.writePackageJSON(packageJSONData);
            break;


          case 'semVer':
            packageJSONData.version = value;
            dependencies.writePackageJSON(packageJSONData);

            configXMLData.widget.$.version = value;
            dependencies.writeConfigXML(configXMLData);
            break;
        }
        
        return;
      } else {
        if (typeArg1 === 'string') { // @variation 2: Get a specific property
          if (!PackageJSONPropTypes.hasOwnProperty(arg1 as string)) throw Error(`prop "${ name }" is not allowed to access`);
          else {
            const name = arg1 as keyof ProjectInfoData;
            let value: any;

            switch (name) {
              case 'name':
                value = configXMLData.widget.name;
                if (typeof value !== 'string') throw Error(`"configXMLData.widget.name" should be a string`);
                break;

              case 'semVer':
                value = packageJSONData.version;
                if (typeof value !== 'string') throw Error(`"packageJSONData.version" should be a string`);
                break;

              case 'packageName':
                value = packageJSONData.name;
                if (typeof value !== 'string') throw Error(`"packageJSONData.name" should be a string`);
                break;
            }

            return value;
          }
        } else if (isPlainObject(arg1)) { // @variation 4: Set multiple properties
          await Promise.all(
            Object
            .entries(arg1)
            .map(
              async ([key, value]) => await projectInfo(key as any, value)
            )
          );

          return;
        } else {
          throw Error(`Inable to run, please check its arguments: projectInfo(${ arg1 }, ${ arg2 })`);
        }
      }
    }
  });
}

