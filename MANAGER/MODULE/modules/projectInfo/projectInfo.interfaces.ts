import { ReadPackageJSON, WritePackageJSON } from '../projectInfo/packageJSON';
import { ReadConfigXML,   WriteConfigXML   } from '../projectInfo/configXML';





export interface ProjectInfoData {
  name: string;
  /**
   * The name in KebabCase that follow npm's rules
   * https://docs.npmjs.com/files/package.json
   */
  packageName: string;
  semVer: string;
}
export type ProjectInfoPropName = keyof ProjectInfoData;






export interface ProjectInfoDependencies {
   readPackageJSON: ReturnType<typeof  ReadPackageJSON>;
  writePackageJSON: ReturnType<typeof WritePackageJSON>;

     readConfigXML: ReturnType<typeof    ReadConfigXML>;
    writeConfigXML: ReturnType<typeof   WriteConfigXML>;
}
