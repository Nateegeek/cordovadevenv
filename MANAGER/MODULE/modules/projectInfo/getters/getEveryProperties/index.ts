import { ProjectInfoData } from "../../projectInfo.interfaces";
import { PackageJSONData } from "../../packageJSON";
import { ConfigXMLData } from "../../configXML";





export function returnEveryProperties(packageJSONData: PackageJSONData, configXMLData: ConfigXMLData) {
  const { name: packageName, version: semVer } = packageJSONData;

  const completeProjectInfo: ProjectInfoData = {
    name: configXMLData.widget.name,
    packageName,
    semVer
  };

  return completeProjectInfo;
}
