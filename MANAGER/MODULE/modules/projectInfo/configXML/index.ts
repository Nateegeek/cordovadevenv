import fs from 'fs';
import { Builder, parseStringPromise } from 'xml2js';

import { Context } from '../../common/common.interfaces';





export interface ConfigXMLData {
  widget: {
    name: string;
    $: {
      version: string;
    }
  }
};




/**
 * Factory function that returns readConfigXML()
 * @param context 
 */
export function ReadConfigXML(context: Context): typeof readConfigXML {
  return readConfigXML.bind({ context });
}








async function readConfigXML(): Promise<ConfigXMLData> {
  const { config } = this.context;

  const configPath = 'app.configXML.path';
  const path = config.get(configPath);

  if (typeof path !== 'string') throw Error(`"${ configPath }" must be a string`);

  const fileContent = fs.readFileSync(path, 'utf-8');
  const xmlData = await parseStringPromise(fileContent);

  return xmlData;
}










/**
 * Factory function that returns writeConfigXML()
 * @param context 
 */
export function WriteConfigXML(context: Context): typeof writeConfigXML {
  return writeConfigXML.bind({ context });
}








function writeConfigXML(jsonData: ConfigXMLData) {
  const { config } = this.context;

  const configPath = 'app.configXML.path';
  const path = config.get(configPath);

  if (typeof path !== 'string') throw Error(`"${ configPath }" must be a string`);

  const builder = new Builder({ renderOpts: { pretty: true, indent: '    ' }});
  const xml = builder.buildObject(jsonData);

  fs.writeFileSync(path, xml);

  return;
}