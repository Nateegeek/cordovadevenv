export type LogLevel = 'error' | 'warn' | 'info' | 'verbose' | 'debug' | 'silly';




interface Vars {
  filePath?   : string;
  fileContent?: string;

  [key: string]: any;
}



export interface LogObject {
  level:        LogLevel;
  message:      string;
  vars?:        Vars;
  className?:   string
  methodName?:  string;
}
export interface LogObjectWoLevel extends Omit<LogObject, 'level' > {}

export interface   LogParams extends LogObject        {};
export interface DebugParams extends LogObjectWoLevel {};
export interface  WarnParams extends LogObjectWoLevel {};
export interface ErrorParams extends LogObjectWoLevel { error: Error };



export interface ChildParams {
   className?: string;
  methodName?: string;
}

export interface Logger {
    log(params:   LogParams): void;
  // TODO debug() should return params.vars (multi-props) or params.vars[1° key] if there's only 1 prop
  debug(params: DebugParams): any;
   warn(params:  WarnParams): any;
  error(params: ErrorParams): any;

  child: (params: ChildParams) => Logger;
}



export interface Context {
  logger: Logger;
}







export interface FS {
    existsSync: (filePath: string)                   => boolean;
  readFileSync: (filePath: string, encoding: string) => string;
 writeFileSync: (filePath: string,  content: string) => void;
}