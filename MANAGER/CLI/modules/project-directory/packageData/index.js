const fs = require('fs');



const defaultPackagePath = './APP/package.json';



function readPackageData(context) {
  const path = context.config.get('app.packageJSON.path') || defaultPackagePath;
  
  const rawData = fs.readFileSync(path);
  const packageData = JSON.parse(rawData.toString());

  return packageData;
}

exports.readPackageData = readPackageData;




function writePackageData(data, context) {
  const { logger } = context;

  const path = context.config.get('app.packageJSON.path') || defaultPackagePath;
  
  const prettified = JSON.stringify(data, null, 2);

  const old = fs.readFileSync(path).toString();

  const jsdiff = require('diff');
  const diff = jsdiff.diffLines(old, prettified);
  /* 
  const removedParts = diff.filter(part => part.removed);
  const   addedParts = diff.filter(part => part.added  );
  
  removedParts.forEach(part => debugDiffRemoved(part.value.trim()));
    addedParts.forEach(part => debugDiffAdded(  part.value.trim()));
   */
  logger.log({
    level: 'debug',
    message: `write "${ path }"`,
    diff
  });

  fs.writeFileSync(path, prettified);
}

exports.writePackageData = writePackageData;
