const fs = require('fs');


const defaultConfigXMLPath = './APP/config.xml';


async function readConfigXML(context) {
  const path = context.config.get('app.configXML.path') || defaultConfigXMLPath;
  const parseStringPromise = require('xml2js').parseStringPromise;

  const data = fs.readFileSync(path, 'utf-8');
  const parsed = await parseStringPromise(data);

  return parsed;
}

exports.readConfigXML = readConfigXML;



function writeConfigXML(jsonData, context) {
  const { logger } = context;
  const path = context.config.get('app.configXML.path') || defaultConfigXMLPath;
  const { Builder } = require('xml2js');
  const builder = new Builder({ renderOpts: { pretty: true, indent: '    ' }});
  const xml = builder.buildObject(jsonData);

  const oldXML = fs.readFileSync(path).toString();

  const jsdiff = require('diff');
  const diff = jsdiff.diffLines(oldXML, xml);
  
  logger.log({
    level: 'debug',
    message: `write "${ path }"`,
    diff
  });

  fs.writeFileSync(path, xml);
}

exports.writeConfigXML = writeConfigXML;
