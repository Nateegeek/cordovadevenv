const Table = require('cli-table');
const chalk = require('chalk');






async function showAppInfo(context, configXMLData) {
  const defaults = require('lodash.defaults');
  if (!configXMLData) {
    configXMLData = await require('./configXMLData').readConfigXML(context);
  }

  const widget = configXMLData.widget;


  const tableChars = defaults({
    'top':          '═',
    'top-mid':      '╤',
    'top-left':     '╔',
    'top-right':    '╗',
    'bottom':       '═',
    'bottom-mid':   '╧',
    'bottom-left':  '╚',
    'bottom-right': '╝',
    'left':         '║',
    'left-mid':     '╟',
    'mid':          '─',
    'mid-mid':      '┼',
    'right':        '║',
    'right-mid':    '╢',
    'middle':       '│'
  }, context.config.get('table.chars'));

  let showAppInfo = context.config.get('table.appInfo.show');
  showAppInfo = typeof showAppInfo === 'boolean' ? showAppInfo : true;

  if (showAppInfo) {
    const table = new Table({
      chars: tableChars,
      colWidths: context.config.get('table.appInfo.colWidths') || [9, 25, 7, 34]
    });
  
    const platforms = widget.platform.map(p => p.$.name);
    
    const tableData = [
      ['NAME',    widget.name,        'ID',      widget.$.id              ],
      ['VERSION', widget.$.version,   'PLAT.',   platforms.join(', ')     ],
      ['AUTHOR',  widget.author[0]._, 'EMAIL',   widget.author[0].$.email ]
    ].map(tuple => {
      tuple[0] = chalk.bold.green(tuple[0]);
      tuple[2] = chalk.bold.green(tuple[2]);
      return tuple;
    });
  
    table.push(...tableData);
  
    console.log(table.toString());
  }

  




  let showLargeAppInfo = context.config.get('table.largeAppInfo.show');
  showLargeAppInfo = typeof showLargeAppInfo === 'boolean' ? showLargeAppInfo : true;

  if (showLargeAppInfo) {
    const largerTable = new Table({
      chars: tableChars,
      colWidths: context.config.get('table.largeAppInfo.colWidths') || [9, 68]
    });
  
    const largerTableData = [
      ['DESC.',    widget.description ],
    ].map(tuple => {
      tuple[0] = chalk.bold.green(tuple[0]);
      return tuple;
    });
  
    largerTable.push(...largerTableData);
  
    console.log(largerTable.toString());
  }


  
}

exports.showAppInfo = showAppInfo;
