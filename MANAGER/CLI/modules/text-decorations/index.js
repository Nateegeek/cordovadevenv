function printTitle(context) {
  const defaults = require('lodash.defaults');

  const defaultValues = {
    show : true,
    text1: '   CORDOVA',
    text2: '   Dev  Env'
  };
  const { show, text1, text2 } = defaults(defaultValues, context.config.get('main.title'));

  if (!show) return;
  
  const figlet = require('figlet');
  const chalk  = require( 'chalk');

  console.log(
    chalk.yellow(
      figlet.textSync(text1, { font: 'ANSI Shadow', horizontalLayout: 'full' })
    )
  );
  console.log(
    chalk.redBright(
      figlet.textSync(text2, { font: 'ANSI Shadow', horizontalLayout: 'full' })
    )
  );
}

exports.printTitle = printTitle;
