const defaultConfigJSONPath = `./config.json`;

/**
 * "conf" provides configuration data loaded from "config.json"
 * But please use default values in case config file is not present,
 * or in case the property is not set, or set incorrectly.
 */
function initConf(context) {
  const fs     = require('fs');
  const Path   = require('path');
  const Conf   = require('conf');
  const config = new Conf({
    cwd: '.'
  });

  const configJSONPath = defaultConfigJSONPath;

  try {
    const configJSONString = fs.readFileSync(configJSONPath);

    try {
      JSON.parse(configJSONString.toString());
    } catch(e) {
      context.preDebuggerLogs.push({ level: 'error', message: `Couldn't parse "${ Path.resolve(configJSONPath) }".`});
    }
  } catch(e) {
    context.preDebuggerLogs.push({ level: 'warn', message: `Couldn't read "${ Path.resolve(configJSONPath) }". Default config values will be used.`});
  }

  return config;
}

exports.initConf = initConf;
