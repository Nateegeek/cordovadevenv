

// TODO implement graceful shutdown
/**
 * CTRL+C should not produce error
 */
function setupForAGracefulShutdown(context) {
  process.on('exit', () => {
    
  })
}

exports.setupForAGracefulShutdown = setupForAGracefulShutdown;



/**
 * This CLI tool could be ran from "CLI" directory
 * or even from its parent directory.
 * This function changes the working directory to its parent directory.
 */
function setupWorkingDirectory(context) {
  const { preDebuggerLogs } = context;

  const path = require('path');
  const fs   = require('fs');

  const startingPath = process.cwd();
  const dirName = startingPath.split(path.sep).pop().toLowerCase();

  preDebuggerLogs.push({
    level: 'debug',
    message: 'Current working directory',
    var: { startingPath }
  });

  if (dirName !== 'cli') {
    const inParentDir = fs.existsSync(path.join(startingPath, 'CLI'));
    if (inParentDir) {
      process.chdir('./CLI');
    }

    preDebuggerLogs.push({
      level: 'debug',
      message: 'Working directory is changed',
      var: { path: process.cwd() }
    });
  }
}

exports.setupWorkingDirectory = setupWorkingDirectory;
