/**
 * It does change the original "configData"
 * @param {*} configData, it's the result of "await readConfigXML()"
 * @returns {Promise<string>} version
 */
function promptEditVersion(configData, context) {
  const { logger } = context;

  const inquirer = require('inquirer');  

  return new Promise((resolve, reject) => {
    const promptParams = {
      type: 'input',
      name: 'version',
      message: 'Version (SemVer)',
      default: configData.widget.$.version,
      validate: str => {
        const lowercase  = str === str.toLowerCase();
        const woSpace    = str.indexOf(' ') === -1;
        const twoPeriods = (str.match(/\./g) || []).length === 2;

        logger.log({
          level: 'debug',
          message: 'ID validation',
          var: { lowercase, woSpace, twoPeriods }
        });

        return lowercase && woSpace && twoPeriods;
      }
                        
    };

    // @ts-ignore
    inquirer.prompt(promptParams)
    .then(async ({ version }) => {  
      if (version !== configData.widget.$.version) {
        const { writeConfigXML   } = require('../../../project-directory/configXMLData');
        const { readPackageData,
                writePackageData } = require('../../../project-directory/packageData');
  
        configData.widget.$.version = version;
        writeConfigXML(configData, context);
  
        const packageData = readPackageData(context);
        packageData.version = version;
        writePackageData(packageData, context);
      }  
      
      resolve(version);

    }).catch(reject);
  });
}

exports.promptEditVersion = promptEditVersion;
