/**
 * It does change the original "configData"
 * @param {*} configData, it's the result of "await readConfigXML()"
 * @returns {Promise<string>} name
 */
function promptEditName(configData, context) {
  const { logger } = context;

  const inquirer = require('inquirer');

  return new Promise((resolve, reject) => {
    const promptParams = {
      type: 'input',
      name: 'name',
      message: 'Application name',
      default: configData.widget.name[0],
      validate: str => {
        const alphanumeric = /^([a-zA-Z0-9 _-]+)$/.test(str);

        logger.log({
          level: 'debug',
          message: 'Name validation',
          var: { alphanumeric }
        });
        
        return alphanumeric;
      }
    };

    logger.log({ level: 'verbose', message: 'Name prompt is called', var: { promptParams } });

    // @ts-ignore
    inquirer.prompt(promptParams)
    .then(async ({ name }) => {
      logger.log({ level: 'info', message: 'User inputs the Name', var: { name } });

      if (name !== configData.widget.name[0]) {
        const { writeConfigXML } = require('../../../project-directory/configXMLData');
  
        if (name !== configData.widget.name[0]) {
          configData.widget.name = [ name ];
          writeConfigXML(configData, context);
        }
      }

      resolve(name);
    }).catch(reject);
  });
}

exports.promptEditName = promptEditName;
