/**
 * It does change the original "configData"
 * @param {*} configData, it's the result of "await readConfigXML()"
 * @returns {Promise<string>} name
 */
function promptEditKebabName(configData, context) {
  const { logger } = context;

  const inquirer  = require('inquirer');
  const kebabcase = require('lodash.kebabcase');
  const chalk     = require('chalk');
  const { readConfigXML }    = require('../../../project-directory/configXMLData');
  const { readPackageData,
          writePackageData } = require('../../../project-directory/packageData');

  return new Promise(async (resolve, reject) => {
    
    const same = readPackageData(context).name;
    const name = (await readConfigXML(context)).widget.name[0];
    
    const generatedFromName = kebabcase(name);
    const input = chalk.bold(`I'll type it myself`);

    let promptParams = {
      name: 'kebabName',
      message: 'kebabCase version of the name',
      type: 'list',
      choices: [same, input],
      default: same,
      validate: str => {
        const rightCase = str === kebabcase(str);

        return rightCase;
      }
    };

    if (same !== generatedFromName) {
      promptParams.choices.splice(1, 0, generatedFromName);
    }

    logger.log({ level: 'verbose', message: 'KebabName prompt is called', var: { promptParams } });


    // @ts-ignore
    inquirer.prompt(promptParams)
    .then(async ({ kebabName }) => {
      if (kebabName === input) {
        return inquirer.prompt({
          ...promptParams,
          type: 'input'
        }).then(answers => answers.kebabName);
      }
      return kebabName;
    })
    .then(kebabName => {
      logger.log({ level: 'info', message: 'User inputs the KebabName', var: { kebabName } });

      if (kebabName !== same) {
        const packageData = readPackageData(context);
    
        packageData.name = kebabName;
  
        writePackageData(packageData, context);
      }
      
      resolve(kebabName);
    })
    .catch(reject);
  });
}

exports.promptEditKebabName = promptEditKebabName;
