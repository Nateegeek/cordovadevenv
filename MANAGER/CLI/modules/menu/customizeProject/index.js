
async function customizeProject(context) {
  const { readConfigXML } = require('../../project-directory/configXMLData');
  
  const configData = await readConfigXML(context);

  await require('./name'       ).promptEditName(       configData, context);
  await require('./kebabName'  ).promptEditKebabName(  configData, context);
  await require('./id'         ).promptEditID(         configData, context);
  await require('./version'    ).promptEditVersion(    configData, context);
  await require('./description').promptEditDescription(configData, context);
}

exports.customizeProject = customizeProject;
