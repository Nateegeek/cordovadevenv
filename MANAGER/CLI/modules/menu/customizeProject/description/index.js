/**
 * It does change the original "configData"
 * @param {*} configData, it's the result of "await readConfigXML()"
 * @returns {Promise<string>} description
 */
function promptEditDescription(configData, context) {
  const { logger } = context;

  const inquirer = require('inquirer');

  return new Promise((resolve, reject) => {
    const promptParams = {
      type: 'input',
      name: 'description',
      message: 'Description',
      default: configData.widget.description[0]
    };

    logger.log({ level: 'verbose', message: 'Description prompt is called', var: { promptParams } });

    // @ts-ignore
    inquirer.prompt(promptParams)
    .then(async ({ description }) => {
      logger.log({ level: 'info', message: 'User inputs the description', var: { description } });
      
      if (description !== configData.widget.description[0]) {
        const { writeConfigXML   } = require('../../../project-directory/configXMLData');
        const { readPackageData,
                writePackageData } = require('../../../project-directory/packageData');
  
        configData.widget.description[0] = description;
        writeConfigXML(configData, context);
  
        const packageData = readPackageData(context);
        packageData.description = description;
        writePackageData(packageData, context);
      }  
      
      resolve(description);

    }).catch(() => {
      logger.error(`Couldn't prompt for the user to input the "description"`);
    }).finally(() => {
      resolve();
    });
  });
}

exports.promptEditDescription = promptEditDescription;
