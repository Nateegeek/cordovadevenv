/**
 * It does change the original "configData"
 * @param {*} configData, it's the result of "await readConfigXML()"
 * @returns {Promise<string>} ID
 */
function promptEditID(configData, context) {
  const { logger } = context;
  const inquirer = require('inquirer');

  return new Promise((resolve, reject) => {

    const promptParams = {
      type: 'input',
      name: 'ID',
      message: 'Application ID (reverse domain name)',
      default: configData.widget.$.id,
      validate: str => {
        const lowercase  = str === str.toLowerCase();
        const woSpace    = str.indexOf(' ') === -1;
        const twoPeriods = (str.match(/\./g) || []).length === 2;

        logger.log({
          level: 'debug',
          message: 'ID validation',
          var: { lowercase, woSpace, twoPeriods }
        });

        return lowercase && woSpace && twoPeriods;
      }
                        
    };

    logger.log({ level: 'verbose', message: 'ID prompt is called', var: { promptParams } });

    // @ts-ignore
    inquirer.prompt(promptParams)
    .then(async ({ ID }) => {
      logger.log({ level: 'info', message: 'User inputs the ID', var: { ID } });

      if (ID !== configData.widget.$.id) {
        const { writeConfigXML } = require('../../../project-directory/configXMLData');
  
        configData.widget.$.id = ID;
        writeConfigXML(configData, context);
      }
      
      resolve(ID);

    }).catch(reject);
  });
}

exports.promptEditID = promptEditID;
