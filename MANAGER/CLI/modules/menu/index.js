const inquirer = require('inquirer');




async function showMainMenu(context) {
  const customize = 'Customize the project';
  const exit      = 'Exit';
  await inquirer.prompt({
    type: 'list',
    name: 'intent',
    message: 'What would you like to do ?',
    choices: [customize, exit]
  }).then(async ({ intent }) => {
    switch (intent) {
      case customize:
        await require('./customizeProject').customizeProject(context);
        break;
      case exit: process.exit();
    }
  });
}

exports.showMainMenu = showMainMenu;
