
function initWinston(context) {
  const defaults = require('lodash.defaults');
  const winston = require('winston');
  const { format } = winston;
  require('winston-daily-rotate-file');

  const dailyRotateFileTransport = new (winston.transports.DailyRotateFile)(defaults({
    filename: './logs/cordovaDevEnv-%DATE%.log',
    datePattern: 'YYYY-MM-DD-HH',
    zippedArchive: true,
    maxSize: '20m',
    maxFiles: '14d'
  }, context.config.get('debug.winston.dailyRotateFile')));

  const logger = winston.createLogger({
    level: process.env.debugLevel || context.config.get('debug.winston.level') || 'info',
    format: format.json(),
    defaultMeta: {},
    transports: [
      dailyRotateFileTransport
    ]
  });
   
  //
  // If we're not in production then log to the `console` with the format:
  // `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
  //
  if (context.development) {
    const chalk    = require('chalk');
    const padend   = require('lodash.padend');
    const defaults = require('lodash.defaults');

    const prettifyVarFormat = format((info, opts) => {
      const { var: theVar, diff: TheDiff } = info;
      let returns = true;

      const logFrame = defaults({
        header: `[ %LEVEL% ]_`,
        footer: `=`,
        length: 80
      }, context.config.get('debug.logFrame'));

      const headerLineC = logFrame.header.slice(-1);
      const headerTitle = logFrame.header.replace('%LEVEL%', info.level.toUpperCase());
      const header      = chalk.gray.bold(padend(headerTitle, logFrame.length, headerLineC));

      const footerLineC = logFrame.footer.slice(-1);
      const footerTitle = logFrame.footer.replace('%LEVEL%', info.level.toUpperCase());
      const footer      = chalk.gray.bold(padend(footerTitle, logFrame.length, footerLineC));
      
      if (typeof theVar !== 'undefined') {
        console.log([
          '\n\n' +
          header,
          chalk.white(info.message) + '\n',
          Object.entries(theVar)
          .reduce((acc ,[name, value]) =>
            acc + `${ chalk.bold.black.bgYellow(name + ':') }\n${ chalk.yellow(JSON.stringify(value, null, 2)) }\n`
            , ''
          ),
          footer + '\n'
        ].join('\n'));

        returns = false;
      }

      if (typeof TheDiff !== 'undefined') {
        console.log([
          '\n\n' +
          header,
          chalk.white(info.message) + '\n',
          TheDiff.reduce(
            (acc ,line) => {
              if (line.added) {
                acc += `${ chalk.bold.black.bgGreen(' ') }  ${ chalk.green(line.value) }`;
              } else if (line.removed) {
                acc += `${ chalk.bold.black.bgRed(' ') }  ${ chalk.red(line.value) }`;
              }
              return acc;
            }
            , ''
          ),
          footer + '\n'
        ].join('\n'));

        returns = false;
      }

      return returns ? info : false;
    });

    logger.add(new winston.transports.Console({
      format: prettifyVarFormat()
    }));
  }

  logger.silly(`Logger is initialized`);

  context.preDebuggerLogs.forEach(log => logger.log(log));

  return logger;
}

exports.initWinston = initWinston;




/**
 * Send unhandled exceptions to Sentry
 * @param {*} context 
 */
function initSentry(context) {
  if (context.development) return;

  const Sentry = require('@sentry/node');

  const defaultSentryInitParams = { dsn: 'https://3194fdb15e3f4b5ea0a94d7b12ebdd0a@sentry.io/1878151' }
  const sentryInitParams = context.config.get('debug.sentry.init') || defaultSentryInitParams;

  Sentry.init(sentryInitParams);
}

exports.initSentry = initSentry;
