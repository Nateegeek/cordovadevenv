const os       = require('./modules/os');
const Config   = require('./modules/config');
const textDeco = require('./modules/text-decorations');
const projDir  = require('./modules/project-directory');
const menu     = require('./modules/menu');
const debug    = require('./modules/debug');
const clear    = require('clear');











async function start() {
  const development = process.env.NODE_ENV === 'development';
  const production  = !development;

  const context = { preDebuggerLogs: [], production, development };


  os.setupForAGracefulShutdown(context);
  os.setupWorkingDirectory(context);


  const config = context.config = Config.initConf(context);
  

  debug.initSentry(context);
  const logger = context.logger = debug.initWinston(context);


  const defaultLoops = 100;
  let loops;
  try {
    loops = config.get('main.loops');
    if (typeof loops !== 'number' || loops < 0) throw Error();
  } catch(e) {
    logger.warn(`main.loops (config.json) must be a positive number. Default value (${ defaultLoops }) is set.`);
    loops = defaultLoops;
  }
  while (loops--) {
    if (production) clear();
    textDeco.printTitle(      context);
    await projDir.showAppInfo(context);
    await menu.showMainMenu(  context);
  }
}

start();
